#Exam
############################### Q1 ######################
ducumentDtm <- read.csv('document categorization.csv', stringsAsFactors = FALSE)

#Turn type into a factor
ducumentDtm$text <- as.factor(ducumentDtm$text)

#text mining package
install.packages('tm')

#load the package
library('tm')

#turn into a corpus
document_corpus <- Corpus(VectorSource(ducumentDtm$text))

#looking at the corpus which is basicaly list
document_corpus[[1]][[1]]
document_corpus[[1]][[2]]
document_corpus[[2]][[1]]

#remove punctuation
clean_corpus <- tm_map(document_corpus, removePunctuation)
clean_corpus[[1]][[1]]

#remove digits
clean_corpus <- tm_map(clean_corpus, removeNumbers)


#turn to lower case
clean_corpus <- tm_map(clean_corpus, content_transformer(tolower))

#remove stopwords
clean_corpus <- tm_map(clean_corpus, removeWords, stopwords())

clean_corpus[[1]][[1]]

#remove multiple white spaces
clean_corpus <- tm_map(clean_corpus, stripWhitespace)

#generate document term matrix (dtm)
dtm <- DocumentTermMatrix(clean_corpus)
dim(dtm)

dtm[[1]]

frequent_dtm <- DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,1)))
dtm[1:2, 1:6]

#################################### Q2 #################################

dataframe <- data.frame(text=sapply(clean_corpus, identity), 
                        stringsAsFactors=F)  
  
newDFdata <- data.frame(dataframe$text, ducumentDtm$category )
names(newDFdata) <- c("text", "category")



newDFdata$category <- as.factor(newDFdata$category)




#apllying naiive base
install.packages('e1071')
library(e1071)

#note: column 60 is the class
model <- naiveBayes(newDFdata[,-2],newDFdata[,2])

model




###################################### Q3 ##########################################

install.packages('SDMTools')
library(SDMTools)

prediction <- predict(model, newDFdata[,2])
prediction


conv_10 <- function (x) {
  x <- ifelse(x == 'Sport', 1,0)}

pred01 <- sapply(prediction, conv_10)
actual01 <- sapply(newDFdata$category, conv_10)

confusion.matrix(actual01, pred01)




############################################# Q4 ###################

newDFdata$category <- as.numeric(newDFdata$category)
newDFdata$text  <- as.numeric(newDFdata$text)

sub_set <- newDFdata[,1:2]

#Make scaling on the numeric fields NIRMUL!!!
scale_num <- scale(sub_set)
scale_num$text

scalDFdata <- data.frame(scale_num)

convv <- function (x) {
  x <- ifelse(x>0, 1,0)}

scalDFdata$text <- sapply(scalDFdata$text, convv)
scalDFdata$category <- sapply(scalDFdata$category, convv)
#שימוש במודל של רגרסיה לוגיסטית

logmodel <- glm(scalDFdata$category ~ scalDFdata$text,
                data = scalDFdata,
                family="binomial")
summary(logmodel)


########################## Q5 ##################################

predicted <- predict(logmodel, scalDFdata[,-50], type ="response")
predicted
predicted <- sapply(predicted, conv05)

conv05 <- function (x) {
  x <- ifelse(x<=0.5, 1,0)}

actual <- scalDFdata$text
install.packages('SDMTools')
library(SDMTools)

confusion.matrix(actual,predicted)


########################### Q6 ##################################
library(class)

cluData <- read.csv('document categorization.csv', stringsAsFactors = FALSE)

sub_setC <- newDFdata$text


#Make scaling on the numeric fields NIRMUL!!!
scale_numC <- scale(sub_setC)
str(scale_numC)

boxplot(scale_numC, col = "GREEN")

newDFdata$text <- scale_numC
set.seed(111111)

#applying the KMEANS model. 4=K 
clusters <- kmeans(scale_numC,2)

#vizualization of the data
plot(newDFdata$text,  col = clusters$cluster, pch = 20, cex = 2)
points(clusters$centers, col = 'purple', pch = 17, cex = 3)

clusters$iter
clusters$size


